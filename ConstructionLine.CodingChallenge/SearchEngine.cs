﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConstructionLine.CodingChallenge
{
    public class SearchEngine
    {
        private readonly List<Shirt> _shirts;
        public SearchEngine(List<Shirt> shirts)
        {
            _shirts = shirts ?? throw new ArgumentException("List of shirts cannot be null");
        }


        public SearchResults Search(SearchOptions options)
        {
            if (options == null)
                throw new ArgumentException("Search options cannot be null");
            var colorCount = Color.All.ToDictionary(x => x, x => 0);
            var sizeCount = Size.All.ToDictionary(x => x, x => 0);
            var resultShirts = new List<Shirt>();
            foreach (var shirt in _shirts)
            {
                if (!options.Colors.Any() || options.Colors.Contains(shirt.Color))
                    sizeCount[shirt.Size]++;
                if (!options.Sizes.Any() || options.Sizes.Contains(shirt.Size))
                    colorCount[shirt.Color]++;
                if ((!options.Colors.Any() || options.Colors.Contains(shirt.Color)) && (!options.Sizes.Any()|| options.Sizes.Contains(shirt.Size)))
                    resultShirts.Add(shirt);
            }
            return new SearchResults
            {
                Shirts = resultShirts.ToList(),
                ColorCounts = colorCount.Select(x => new ColorCount(){Color = x.Key, Count = x.Value}).ToList(),
                SizeCounts = sizeCount.Select(x => new SizeCount(){Size = x.Key, Count = x.Value}).ToList()
            };
        }
    }
}